import { Component, OnInit } from '@angular/core';
import 'codemirror/mode/python/python';
import 'codemirror/keymap/sublime';
import 'codemirror/addon/fold/indent-fold';
import 'codemirror/addon/fold/foldgutter';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  code = `def foo():
	do_some_stuff()
    here
    return None

class Bar:
	__init__(self):
    if True:
    	print("True")
    else:
        print("False")

this_code_makes_no_sense():
      pass

`;
  config = {
      lineNumbers: true,
      mode: {name: "python", json: true},
      theme: "mbo",
      readOnly: false,
      lineWrapping: true,
      indentUnit: 4,
      indentWithTabs: true,
      keyMap: "sublime",
      foldGutter: true,
      gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
      extraKeys: {"Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); }}
  };
  constructor() { }
  ngOnInit() {
  }
  onFocus(){
    console.log(this.code);
  }

  onBlur(){
    console.log(this.code);
  }
}
