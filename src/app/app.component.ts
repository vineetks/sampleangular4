import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Brand New Angular App';
  name = 'Name';
  constructor(private _appService: AppService) { }
  
  ngOnInit(){
    this.name = this._appService.getName();
  }
  
  getTitle(){
    this.name = this._appService.getName();
  }
}
