import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {

  constructor(private _appService: AppService) { }
  default = 'is-dark';
  ngOnInit() {
  }
  
  setTitle(name: string){
    console.log(name)
    this._appService.setName(name);
  }

}
