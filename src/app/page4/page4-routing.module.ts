import { NgModule } from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import { CommonModule } from '@angular/common';
import{ Page4Component } from './page4.component';
import { Page41Component } from './page41.component';
import { Page42Component } from './page42.component';

const childRoutes: Routes = [
  { path: 'page4', component:  Page4Component, children: [
        {path: '1', component:  Page41Component},
        {path: '2', component:  Page42Component}
  ]}
];

export const page4RoutingComponents = [ Page4Component, Page41Component, Page42Component];
export const routes = RouterModule.forChild(childRoutes)
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(childRoutes)
  ],
  declarations: [],
  exports: [ RouterModule ]
})
export class Page4RoutingModule { }
