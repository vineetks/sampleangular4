import { NgModule } from '@angular/core';
import { CommonModule }   from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import{ Page4RoutingModule, page4RoutingComponents, routes } from './page4-routing.module';

@NgModule({
  declarations: [
    page4RoutingComponents
  ],
  imports: [
    CommonModule,
    Page4RoutingModule
    //,routes
  ],
  providers: []
})
export class Page4Module { }
